ARG BASE_IMG

FROM ${BASE_IMG}

RUN apt-get update  && apt-get install -y python3-tk
RUN apt-get update && apt-get upgrade -y && \
    rm -rf /var/lib/apt/lists/*

RUN mkdir -p /utils && cd /utils && \
    git clone https://github.com/raspberrypi/userland.git && cd userland && ./buildme

WORKDIR /workspace/ros_ws
RUN mkdir -p /workspace/ros_ws/src && cd src && git clone https://github.com/UbiquityRobotics/raspicam_node
RUN apt update && apt install -y libraspberrypi-dev libraspberrypi0 libpigpiod-if-dev
RUN apt update && apt install -y ros-noetic-compressed-image-transport ros-noetic-camera-info-manager ros-noetic-diagnostic-updater ros-noetic-usb-cam
COPY ./requirements.txt /workspace/ros_ws/src/rpi_optical_flow/requirements.txt
RUN pip3 install -r src/rpi_optical_flow/requirements.txt

COPY . /workspace/ros_ws/src/rpi_optical_flow

RUN /bin/bash -c "catkin build"
CMD ["/bin/bash -ci", "roslaunch rpi_optical_flow optical_flow.launch"]
