import unittest

import numpy as np
import pathlib
import sys, os
import matplotlib.pyplot as plt

sys.path.append(str(pathlib.Path().resolve()))

from rpi_optical_flow import filter


class TestOpticalFlow(unittest.TestCase):
    def setUp(self):
        self.__freq = 30
        self.__filt = filter.LowPassFilter(4, 1.1, 2)

    def testFilter(self):
        FINAL_TIME=10.0
        time = np.linspace(0.0, FINAL_TIME, int(self.__freq*FINAL_TIME))
        x = np.cos(time)+np.random.normal(0.0, 0.05, size=int(self.__freq*FINAL_TIME))
        y = np.sin(time)+np.random.normal(0.0, 0.05, size=int(self.__freq*FINAL_TIME))
        output = []
        for i in range(0, time.shape[0]):
            vec = np.array([x[i], y[i]])
            filtered_vec = self.__filt.filter(vec)
            self.assertEqual(filtered_vec.shape[0], 2)
            output.append(filtered_vec)
        
        result = np.concatenate(output, axis=0).reshape(time.shape[0], 2)
        plt.figure()
        plt.subplot(211)
        plt.plot(time, x, 'bo', time, result[:,0], 'k')
        plt.subplot(212)
        plt.plot(time, y, 'bo', time, result[:,1], 'k')
        plt.savefig("result.jpg")


if __name__ == '__main__':
    unittest.main()