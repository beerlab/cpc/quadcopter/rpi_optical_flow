# rpi_optical_flow

## Description
This is a fork of [pi_drone_ros](https://github.com/Razbotics/pi_drone_ros/tree/master) package. 

**rpi_optical_flow** -- ros package, for calculating optical (drone velocity) using [MotionVectors](https://github.com/UbiquityRobotics/raspicam_node)

## Building local changes
Just use docker compose:

    docker compose -f docker-compose.local.yaml build 

