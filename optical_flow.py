#!/usr/bin/env python3

'''
   Copyright (C) 2023  ./lab_449

   THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
   INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
   PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
   FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR 
   OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
   DEALINGS IN THE SOFTWARE.

   Author: Antipov Vladislav <vantipovt@gmail.com>
'''

import rospy
import numpy as np
from rpi_optical_flow import optical_flow_node


def main():

    orientation_sub_topic = rospy.get_param("imu_topic", "/mavros/imu/data")
    motion_data_sub_topic = rospy.get_param("motion_data_sub_topic", "/raspicam_node/motion_vectors")
    range_finder_sub_topic = rospy.get_param("rangefinder_topic", "/mavros/distance_sensor/rangefinder_pub")
    body_twist_pub_topic = rospy.get_param("output_velocity_topic", "/rpi_optical_flow/twist")
    camera_fov_p = np.array(rospy.get_param("camera_fov_p", [500.0, 500.0]))
    camera_mount_rotation = np.array(rospy.get_param("camera_mount_rotation", [0.0, 0.0, 0.0, 1.0]))

    rate = 50.0

    of_node  = optical_flow_node.OpticalFlowNode(
        "rpi_optical_flow",
        orientation_sub_topic,
        motion_data_sub_topic,
        range_finder_sub_topic,
        body_twist_pub_topic,
        camera_fov_p,
		camera_mount_rotation,
        rate=rate
    )
    
    of_node.loop()


if __name__ == "__main__":
    main()