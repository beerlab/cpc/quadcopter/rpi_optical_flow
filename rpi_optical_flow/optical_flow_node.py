'''
   Copyright (C) 2023  ./lab_449

   THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
   INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
   PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
   FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR 
   OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
   DEALINGS IN THE SOFTWARE.

   Author: Antipov Vladislav <vantipovt@gmail.com>

   This code uses work from https://github.com/Razbotics/pi_drone_ros/tree/master 
'''

import numpy as np
from scipy.spatial.transform import Rotation as R

import math
import rospy
import rostopic

from sensor_msgs.msg import Imu
from geometry_msgs.msg import TwistStamped

from sensor_msgs.msg import Image
from raspicam_node.msg import MotionVectors

from scipy.spatial.transform import Rotation as R

from rpi_optical_flow.filter import LowPassFilter
from rpi_optical_flow.optical_flow_estimators import ZVelocityEstimator, OpticalFlowVelocityEstimator
from rpi_optical_flow.motion_vector_estimator import USBCamMotionVectorEstimator, RaspiCamMotionVectorEstimator

np.set_printoptions(precision=3)

class OpticalFlowNode():
	def __init__(self,
	    	node_name: str = "optical_flow_node",
	    	orientation_sub_topic: str = "/mavros/imu/data",
			motion_sub_topic: str = "/raspicam_node/motion_vectors",
			range_finder_sub_topic: str = "/mavros/distance_sensor/rangefinder_pub",
			body_twist_pub_topic: str = "/rpi_optical_flow/twist",
			fov_p: np.ndarray = np.array([500.0, 500.0]),
			camera_mount_rotation: np.ndarray = np.array([0., 0., 0., 1.]),
			rate: float = 50.0,
		):
		rospy.init_node(node_name)
		self.__ros_rate = rospy.Rate(rate)

		rospy.Subscriber(orientation_sub_topic, Imu, self.__orientation_callback)

		self.__camera_mount_rot = R.from_quat(camera_mount_rotation)
		self.__body_twist_pub = rospy.Publisher(body_twist_pub_topic, TwistStamped, queue_size = 1)

		# Data from IMU
		self.__current_orientation = R.identity()
		self.__current_angular_velocity = np.array([0.0, 0.0, 0.0], dtype=float)

		# Altitude velocity estimator
		self.__z_vel_estimator = ZVelocityEstimator(range_finder_sub_topic)
		# Velocity estimator
		self.__xy_vel_estimator = OpticalFlowVelocityEstimator(fov_p)

		# Initialize motion vector estimator
		self.__motion_vector_estimator = None
		TopicType, _, _ = rostopic.get_topic_class(motion_sub_topic)
		if TopicType == Image:
			self.__motion_vector_estimator = USBCamMotionVectorEstimator(motion_sub_topic)
			print("# Intialize USB Camera Optical flow estimator (KL)")
		elif TopicType == MotionVectors:
			self.__motion_vector_estimator = RaspiCamMotionVectorEstimator(motion_sub_topic)
			print("# Intialize RaspiCam optical flow estimator (MotionVectors)")
		else:
			raise(RuntimeError("Cannot subscribe on topic: ", motion_sub_topic))


		self.__filter_vel = LowPassFilter(2, 5, rate)
		self.__current_linear_velocity = np.array([0.0, 0.0, 0.0], dtype=float)
		
	
	def __orientation_callback(self, imu_data: Imu):
		# TODO: for using without Gimbal
		self.__current_orientation = R.from_quat(
			[
				imu_data.orientation.x,
				imu_data.orientation.y,
				imu_data.orientation.z,
				imu_data.orientation.w
			]
		)
		self.__current_angular_velocity = np.array([imu_data.angular_velocity.x, imu_data.angular_velocity.y, imu_data.angular_velocity.z])
	
	def loop(self):
		while not rospy.is_shutdown():
			self.__current_motion_vector = self.__motion_vector_estimator.current_motion_vector
			dt = self.__motion_vector_estimator.dt
			self.__current_linear_velocity = np.concatenate([
				self.__xy_vel_estimator.calc_velocity(
					self.__current_motion_vector,
					self.__z_vel_estimator.altitude+1.0,
					dt,
					np.array([0.1, 0.0, 0.0])
				),
				np.array([self.__z_vel_estimator.z_velocity])
			])
			self.__current_linear_velocity = self.__camera_mount_rot.as_matrix() @ self.__filter_vel.filter(self.__current_linear_velocity)
			print("Receive and filt velocity: ", self.__current_linear_velocity)
			self.__publish_twist()
			self.__ros_rate.sleep()

	def __publish_twist(self):
		twist_msg = TwistStamped()
		twist_msg.header.stamp = rospy.Time.now()
  
		twist_msg.twist.linear.x = float(self.__current_linear_velocity[0])
		twist_msg.twist.linear.y = float(self.__current_linear_velocity[1])
		twist_msg.twist.linear.z = float(self.__current_linear_velocity[2])

		twist_msg.twist.angular.x = float(self.__current_angular_velocity[0])
		twist_msg.twist.angular.y = float(self.__current_angular_velocity[1])
		twist_msg.twist.angular.z = float(self.__current_angular_velocity[2])
		self.__body_twist_pub.publish(twist_msg)
