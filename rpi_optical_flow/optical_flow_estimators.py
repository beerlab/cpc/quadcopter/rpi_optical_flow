
import numpy as np
from scipy.spatial.transform import Rotation as R
import numpy as np
import rospy
from sensor_msgs.msg import Range


class ZVelocityEstimator():
	def __init__(self, 
		range_finder_sub_topic: str = "/mavros/distance_sensor/rangefinder_pub",
		max_dt: float = 0.01
	):
		rospy.Subscriber(range_finder_sub_topic, Range, self.__altitude_callback)
		self.__z_velocity = 0.0
		self.__max_dt = max_dt
		self.__last_altitude = 0.0
		self.__current_altitude = 0.0

		self.__current_time_altitude = rospy.Time.now().to_sec()
		self.__last_time_altitude = self.__current_time_altitude
    
	def __altitude_callback(self, range_data: Range):
		self.__last_time_altitude = self.__current_time_altitude
		self.__current_time_altitude = rospy.Time.now().to_sec()

		self.__last_altitude = self.__current_altitude
		self.__current_altitude =  float(range_data.range)

		dt = self.__current_time_altitude - self.__last_time_altitude

		self.__z_velocity = (self.__current_altitude - self.__last_altitude)/max(self.__max_dt, dt)
		# print("Receive and filt altitude: ", self.__current_altitude)
	
	@property
	def z_velocity(self):
		return self.__z_velocity

	@property
	def altitude(self):
		return self.__current_altitude

class OpticalFlowVelocityEstimator():
	def __init__(self, fov_p: np.ndarray):
		self.__fov_p = fov_p
    
	def calc_velocity(
		self,
		motion_vector: np.ndarray,
		altitude: float,
		dt: float,
		ang_vel: np.ndarray = np.array([0.0, 0.0, 0.0])
	) -> np.ndarray:
		xy_vel = np.array([
			np.sin(np.arctan2(motion_vector[0],self.__fov_p[0])/dt + ang_vel[1])*altitude,
			np.sin(np.arctan2(motion_vector[1],self.__fov_p[1])/dt - ang_vel[0])*altitude
		])
		return xy_vel