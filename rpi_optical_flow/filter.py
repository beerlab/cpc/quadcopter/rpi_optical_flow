from scipy import signal
import numpy as np


class LowPassFilter():
  def __init__(self, order: int = 3, freq: float = 5, fs: float =50):
    self.__x = None
    self.__y = None
    self.__order = order
    b, a = signal.bessel(order, Wn=freq, fs=fs)
    self.__A, self.__B, self.__C, self.__D = signal.tf2ss(b, a)

  def filter(self, u: np.ndarray):
    if self.__x is None:
      self.__x = np.zeros((self.__order, u.shape[0]))
      self.__y = u.copy()
      self.__x[0,:] = u.reshape(1, -1)
      return u
    for i in range(0, u.shape[0]):
      self.__x[:,i] = self.__A@self.__x[:,i] + (self.__B*u[i]).flatten()
      self.__y[i] = self.__C@self.__x[:,i] + (self.__D*u[i]).flatten()
    return self.__y

