from abc import abstractmethod, ABC
import numpy as np
from scipy.spatial.transform import Rotation as R

from raspicam_node.msg import MotionVectors
from sensor_msgs.msg import Image

from cv_bridge import CvBridge
from sensor_msgs.msg import Image
import cv2

import rospy

MAX_DT = 0.005

class MotionVectorEstimator(ABC):
	def __init__(
		self, 
		sub_topic: str = "/raspicam_node/motion_vectors"
	):
		# Motion Vectors and velocity estimation 
		self._current_motion_vector = np.array([0.0, 0.0], dtype=float)
		self._last_time_motion = rospy.Time.now().to_sec()
		self._current_time_motion = self._last_time_motion
		self._dt = MAX_DT
		self._sub_topic = sub_topic


	@abstractmethod
	def _motion_vectors_estim_callback(self, data: MotionVectors or Image):
		pass

	@property
	def current_motion_vector(self):
		return self._current_motion_vector
	
	@property
	def dt(self):
		return self._dt


class RaspiCamMotionVectorEstimator(MotionVectorEstimator):
	def __init__(
		self, 
		sub_topic: str = "/raspicam_node/motion_vectors"
	):
		super().__init__(sub_topic)
		rospy.Subscriber(self._sub_topic, MotionVectors, self._motion_vectors_estim_callback)

	def _motion_vectors_estim_callback(self, motion_vectors_data: MotionVectors):
		x_vectors = np.array(motion_vectors_data.x)
		y_vectors = np.array(motion_vectors_data.y)
		x_vectors = x_vectors[np.nonzero(x_vectors)]
		y_vectors = y_vectors[np.nonzero(y_vectors)]
		if len(x_vectors) == 0:
			x_vectors = np.append(x_vectors,[0])
		if len(y_vectors) == 0:
			y_vectors = np.append(y_vectors,[0])
		self._current_motion_vector[0] = -np.average(y_vectors) #optical flow is relative to quadrotor
		self._current_motion_vector[1] = -np.average(x_vectors) # x forward    y left

		# Calculate time delta
		self._last_time_motion = self._current_time_motion
		self._current_time_motion = rospy.Time.now().to_sec()
		self._dt = max(MAX_DT, self._current_time_motion - self._last_time_motion)


class LKTracker(object):
	""" Class for Lucas-Kanade tracking with pyramidal optical flow."""
	def __init__(self):
		self.features = []

		self.__lk_params = dict(winSize=(15,15),maxLevel=2,
			criteria=(cv2.TERM_CRITERIA_EPS | cv2.TERM_CRITERIA_COUNT,10,0.03))
		self.__subpix_params = dict(zeroZone=(-1,-1),winSize=(10,10),
			criteria = (cv2.TERM_CRITERIA_COUNT | cv2.TERM_CRITERIA_EPS,20,0.03))
		self.__feature_params = dict(maxCorners=500,qualityLevel=0.01,minDistance=10)

	def detect_points(self, image: cv2.Mat):
		# search for good points
		features = cv2.goodFeaturesToTrack(image, **self.__feature_params)
		# refine the corner locations
		cv2.cornerSubPix(image, features, **self.__subpix_params)
		self.features = features
		self.prev_image = image
	

	def calc(self, image: cv2.Mat):
		if len(self.features) < 4:
			self.detect_points(image)
			print("recalc")
		return self.flow(image)
			

	def flow(self, image: cv2.Mat):
		prev_features = np.array(self.features).reshape((-1,2))
		tmp = np.float32(self.features).reshape(-1, 1, 2)
		# calculate optical flow
		features,status,track_error = cv2.calcOpticalFlowPyrLK(
			self.prev_image,
			image,
			tmp,
			None,**self.__lk_params
		)
		# remove points lost
		self.features = [p for (st,p) in zip(status,features) if st]
		# clean tracks from lost points
		features = np.array(features).reshape((-1,2))
		
		self.prev_image = image
		return features-prev_features
		

class USBCamMotionVectorEstimator(MotionVectorEstimator):
	def __init__(
		self, 
		sub_topic: str = "/raspicam_node/motion_vectors"
	):
		super().__init__(sub_topic)
		self.__cv_image = None
		self.__LKTracker = LKTracker()
		
		self.__cv_bridge = CvBridge()
		rospy.Subscriber(self._sub_topic, Image, self._motion_vectors_estim_callback)

	def _motion_vectors_estim_callback(self, data: Image):
		self.__cv_image = cv2.cvtColor(
      		self.__cv_bridge.imgmsg_to_cv2(data, desired_encoding='bgr8'),
			cv2.COLOR_BGR2GRAY
		)
		flow = self.__LKTracker.calc(self.__cv_image)

		x_vectors = flow[:,0]
		y_vectors = flow[:,1]

		if len(x_vectors) == 0:
			x_vectors = np.append(x_vectors,[0])
		if len(y_vectors) == 0:
			y_vectors = np.append(y_vectors,[0])
		self._current_motion_vector[0] = np.average(y_vectors) # optical flow is relative to quadrotor
		self._current_motion_vector[1] = np.average(x_vectors) # x forward    y left
		# print(self._current_motion_vector)

		# Calculate time delta
		self._last_time_motion = self._current_time_motion
		self._current_time_motion = rospy.Time.now().to_sec()
		self._dt = max(MAX_DT, self._current_time_motion - self._last_time_motion)


